\include "./sjs_drumconfig.ly"


\header{
	title = "Tick Tick Boom"
	composer = "The Hives"
}

boom = \drummode {
		<<
			{ s2. cymca4 }
			\\
			{ r2 r8 bd8 sn4 }
		>>
}
earlybridge = \drummode {
		<<
			{ hho4. cymca8 cymca8 }
			\\
			{ bd8 sn16 sn16 sn8 bd8 bd8 sn16 sn sn sn sn sn  }
		>>
}
closedverse = \drummode {
	<<
		{ hhc8 hh8 hh hh hh hh hh hh }
		\\
		{ bd4 sn4 bd8 bd8 sn4 }
	>>
}
openversewithcrash = \drummode {
	<<
		{ cymca8 hho8 hh hh hh hh hh hh }
		\\
		{ bd4 sn4 bd8 bd8 sn4 }
	>>
}
openverse = \drummode {
	<<
		{ hho8 hh hh hh hh hh hh hh }
		\\
		{ bd4 sn4 bd8 bd8 sn4 }
	>>
}

verse = \drummode {
	\repeat unfold 2 { \closedverse } 
	\openversewithcrash
    \openverse
}

chorus = \drummode {
	<<
		{ \repeat unfold 8 cymca8  }
		\\
		{ bd8 bd sn8 sn bd8 bd8 sn4 }
	>>	
}

chorusend = \drummode {
	<<
		{ \repeat unfold 8 cymca8  
           sn4 s2.}
		\\
		{ bd8 bd sn8 sn bd8 sn8 sn sn16 sn16 
          bd4 s2. }
	>>
    \boom
}

bridgeone = \drummode {
	<<
		{ hho8  }
		\\
		{ bd8 \repeat unfold 8 { sn16 } bd8 \flam sn8 \flam sn8 }
	>>
}

\new DrumStaff {
  	\set DrumStaff.drumStyleTable = #(alist->hash-table sjsdrums)

	\drummode {
		\boom
		\repeat unfold 3
		<<
			{ hho8 hh8 hh hh hh hh hh hh }
			\\
			{ bd4 sn4 bd8 bd8 sn4 }
		>>
		\earlybridge

		\repeat volta 2 { 
			\verse 
			\repeat unfold 2 { \closedverse } 
			\openversewithcrash }
		\alternative {
			{ \openverse }
			{
		<<
			{ hho8 s4  hh8 hh hh | }
			\\
			{ bd8 sn16 sn16 sn4 bd8 bd8 sn8 sn16 sn16 }
		>>
			}
			
		}

		\repeat unfold 2 { \chorus }
		
		\openversewithcrash 
		\repeat unfold 4 \openverse
		\bridgeone
		\repeat unfold 4 { \chorus }
		\chorusend

		\boom
		\openversewithcrash
		\repeat unfold 4 { \openverse }

		{ bd8 sn16\< \repeat unfold 12 sn16 sn16\! sn4 r4 r2 r1}

		\openversewithcrash \openverse
		\verse
		\repeat unfold 7 \openverse
		\bridgeone
		\repeat unfold 4 { \chorus }
		\chorusend

		r1 r1

		\repeat unfold 3
		<<
			{ s8 s8 hh8 s8 s8 s8 hh8 s8 }
			\\
			{ bd8 bd8 s8 bd8 bd8 bd8 s8 bd8  }
		>>
		<<
			{ s8 s8 hh8 s8 s8 s8 hh8 hh8 }
			\\
			{ bd8 bd8 s8 bd8 bd8 bd8 s4  }
		>>
		\repeat unfold 4
		<<
			{ s8 s8 hh8 s8 s8 s8 hh8 s8 }
			\\
			{ bd8 bd8 s8 bd8 bd8 bd8 s8 bd8  }
		>>
		\repeat unfold 2
		<<
			{ bd8 bd8 sn8 bd8 bd8 bd8 sn8 bd8  }
			{ lowtom8 lowtom8 s8 lowtom8 lowtom8 lowtom8 s8 lowtom8  }
		>>
		<<
			{ cymca4 }
			\\
			{ << bd4 lowtom4 >> }
		>>
		r4 r2
		{ r2 r4 << himidtom4 lowtom4 >> }
		\openversewithcrash
		\repeat unfold 2 \openverse
		\earlybridge

		<<
			{ cymca8 hho8 hh hh hh sn16 sn 
			  sn16 sn sn sn
			  hightom16 hightom hightom hightom 
			  himidtom16 himidtom himidtom himidtom
			  lowmidtom16 lowmidtom lowmidtom lowmidtom
			  lowtom16 lowtom lowtom lowtom }
			\\
			{ bd4 sn4 bd8 bd8 bd bd
			  bd bd bd bd bd bd bd bd
			}
		>>
		<<
			{ cymca8 hho8 hh hh hh 
			  sn16 sn sn hightom16 himidtom16 sn16 
			  sn hightom16 himidtom16 sn16
			  sn16 lowmidtom16 lowmidtom sn16
			  sn16 lowmidtom16 lowtom16 lowtom16
			  sn16 lowtom lowtom lowtom
			}
			\\
			{ bd4 sn4 bd8 bd8 bd bd
			  bd bd bd bd bd bd bd bd
			}
		>>
		<<
			{ cymca8 hho8 hh hh hh sn16 sn 
			  sn16 sn sn sn
			  hightom16 hightom hightom hightom 
			  himidtom16 himidtom himidtom himidtom
			  lowmidtom16 lowmidtom lowmidtom lowmidtom
			  cymca8 cymca }
			\\
			{ bd4 sn4 bd8 bd8 bd bd
			  bd bd bd bd bd bd bd bd
			}
		>>
		<<
			{ cymca8 cymca cymca cymca cymca 
			  sn16 sn sn hightom16 himidtom16 sn16 
			  sn hightom16 himidtom16 himidtom
			  lowtom16 lowtom sn16 sn16
			  sn16 sn sn sn
			  \flam sn8 \flam sn8 
			}
			\\
			{ bd4 sn4 bd8 bd8 bd bd
			  bd bd bd bd bd bd s8 s8
			}
		>>

		\repeat unfold 10 { \chorus }
		\chorusend
		


	}
}


\version "2.16.2"  % necessary for upgrading to future LilyPond versions.
